# Polish REGON Internet Database BIR1.1

The BIR1.1 (Baza Internetowa REGON v1.1) API (https://wyszukiwarkaregon.stat.gov.pl/appBIR/index.aspx).

## Installation

The API client can be installed via [Composer](https://github.com/composer/composer).

In your composer.json file:

```json
{
    "require": {
        "sj_royd/regon_api": "~1.0"
    }
}
```
or using a command line

```
composer require sj_royd/regon_api
```

## Basic Usage

### Default dates format

Default data dates format is `yyyy-mm-dd`.
It is possible to change the dates format:
```php
<?php

require_once 'vendor/autoload.php';

use \SJRoyd\GUS\RegonApi\Date;

Date::setFormat('d.m.Y'); // format dd.mm.yyyy
```

### Login

#### Test mode

```php
<?php

require_once 'vendor/autoload.php';

use SJRoyd\GUS\RegonApi\Exception\KeyMissingException;
use SJRoyd\GUS\RegonApi\BIRService;

$userKey = null; // In the test mode any key is accepted

$api = new BIRService(true);
try {
    $api->login($userKey);
} catch (KeyMissingException $e) {
}
```

#### Production mode

```php
<?php

require_once 'vendor/autoload.php';

use SJRoyd\GUS\RegonApi\BIRService;

$userKey = 'my-unique-key'; // User key obtained from the GUS

$api = new BIRService();
$api->login($userKey);
```

### Search units

```php
<?php

use SJRoyd\GUS\RegonApi\Enum\SearchType;
use SJRoyd\GUS\RegonApi\Exception\InvalidSearchTypeException;
use SJRoyd\GUS\RegonApi\Exception\NotFoundException;
use SJRoyd\GUS\RegonApi\Exception\NumberMissingException;
use SJRoyd\GUS\RegonApi\Exception\TooManyNumbersException;

try {
    $searchBy = SearchType::NIP;
    $searchNumber = '5261040828'
    $units = $api->search($searchBy, $searchNumber);
} catch (InvalidSearchTypeException $e) {
} catch (NotFoundException $e) {
} catch (NumberMissingException $e) {
} catch (TooManyNumbersException $e) {
}
```

#### Search types

- `SearchType::NIP` number param as a string
- `SearchType::NIPS` numbers param as an array of strings or string separated by new line, space, dot, coma or semicolon
- `SearchType::REGON` number param as a string
- `SearchType::REGONS` numbers param as an array of strings or string  (...)
- `SearchType::KRS` number param as a string
- `SearchType::KRSES` numbers param as an array of strings or string  (...)
- `SearchType::REGONS9` numbers param as an array of strings or string (...)
- `SearchType::REGONS14` numbers param as an array of strings or string (...)

### Reports

```php
<?php

use SJRoyd\GUS\RegonApi\Enum\ReportType;
use SJRoyd\GUS\RegonApi\Exception\CivilLawPartnershipException;
use SJRoyd\GUS\RegonApi\Exception\NotFoundException;
use SJRoyd\GUS\RegonApi\Exception\PkdException;
use SJRoyd\GUS\RegonApi\Exception\ReportException;

$regon = '';
$reportType = ReportType::PHYSIC_GENERAL;
try {
    $api->getFullReport($regon, $reportType);
} catch (CivilLawPartnershipException $e) {
} catch (NotFoundException $e) {
} catch (PkdException $e) {
} catch (ReportException $e) {
}
```

#### List of report types

| $unit<br>type | $unit<br>silodId | report type                        | desc                                                                                                                    |
|------------|---------|------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| F          | 1/2/3   | ReportType::PHYSIC_GENERAL                     | Data of a natural person common to all their activities.                                                                |
| F          | 1       | ReportType::PHYSIC_CEIDG                       | Data on the activities registered in the CEIDG, including the address of this activity.                                 |
| F          | 2       | ReportType::PHYSIC_AGRICULTURAL                | Data on agricultural activities; including agricultural business address.                                               |
| F          | 3       | ReportType::PHYSIC_OTHER                       | Data on activities other than CEIDG and agricultural (bailiff, notary, agrotourism) including business address.         |
| F          | 4       | ReportType::PHYSIC_DELETED                     | Data on operations deleted from REGON before 11.11.2014 (i.e. in the previous IT system).                               |
| F          | 1/2/3   | ReportType::PHYSIC_PKD                         | List of PKD codes for the entity of a natural person                                                                    |
| F          | 1/2/3   | ReportType::PHYSIC_LOCALS_LIST                 | List of local units (e.g. company branches) registered for the entity of a natural person                               |
| F          | 1/2/3   | ReportType::PHYSIC_LOCAL (14-digits REGON)     | Data of the local unit of the entity of a natural person                                                                |
| F          | 1/2/3   | ReportType::PHYSIC_LOCAL_PKD (14-digits REGON) | List of PKD codes for the local unit of a natural person                                                                |
| P          |         | ReportType::LAW                                | Legal entity data; Note: Reports for a legal entity also regarding a civil law partnership which is not a legal entity. |
| P          |         | ReportType::LAW_PKD                            | List of PKD codes for the entity of a legal person                                                                      |
| P          |         | ReportType::LAW_LOCALS_LIST                    | List of local units (e.g. company branches) registered for a legal entity                                               |
| LP         |         | ReportType::LAW_LOCAL (14-digits REGON)        | Data of the local unit of the entity of the legal person                                                                |
| LP         |         | ReportType::LAW_LOCAL_PKD (14-digits REGON)    | List of PKD codes for the local unit of the entity of a legal person                                                    |
| P          |         | ReportType::LAW_SC_COLLABORATORS               | List of partners in a civil law partnership (only S.C.) Note: REGON has been registering partners since 2012. (For companies established before 2012 and not updated - there is no data in REGON about partners). |

Note: Not every law unit is a SC (civil law partnership) and this report can throws the CivilLawPartnershipException.

### Automatically get relevant reports for a specific unit

```php
<?php

use SJRoyd\GUS\RegonApi\Exception\NotFoundException;
use SJRoyd\GUS\RegonApi\Exception\PkdException;
use SJRoyd\GUS\RegonApi\Exception\ReportException;

try {
    foreach($units as $unit){
        $unit->getReports();
    }
} catch (NotFoundException $e) {
} catch (PkdException $e) {
} catch (ReportException $e) {
}
```

Every reports will save to the reports property of the unit in.

| $unit<br>type | $unit<br>silosId | reports                       |
|------------|---------------|-------------------------------------|
| F          | 1             | general, ceidg, pkd, locals         |
| F          | 2             | general, agricultural, pkd, locals  |
| F          | 3             | general, other, pkd, locals         |
| F          | 4             | deleted                             |
| LF         | 1/2/3         | general, pkd                        |
| P          |               | general, pkd, locals, collaborators |
| LP         |               | general, pkd                        |

### GetValue

```php
<?php

use SJRoyd\GUS\RegonApi\Enum\Value;

$value = $api->getValue(Value::SERVICE_STATUS);
$value->getMessage();
```