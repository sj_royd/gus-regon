<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

class PKD
{

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $isMain;

    /**
     * @var int|null
     */
    public $silosId;

    /**
     * @var string|null
     */
    public $silosSymbol;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $deletedDate;

    /**
     * @var bool
     */
    public $isActive = true;

    /**
     * @param   string  $code
     */
    public function setFizPkdKod($code)
    {
        $this->code = $code;
    }

    /**
     * @param   string  $name
     */
    public function setFizPkdNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   bool|null  $flag
     */
    public function setFizPkdPrzewazajace($flag)
    {
        $this->isMain = (bool)$flag;
    }

    /**
     * @param   int|null  $id
     */
    public function setFizSilosID($id)
    {
        $this->silosId = $id;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizSilosSymbol($code)
    {
        $this->silosSymbol = $code;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizDataSkresleniaDzialalnosciZRegon($date)
    {
        $this->deletedDate = $date;
        if($date){
            $this->isActive = !Date::$now->diff($date)->invert;
        }
    }

    /**
     * @param   string  $code
     */
    public function setLokfizPkdKod($code)
    {
        $this->code = $code;
    }

    /**
     * @param   string  $name
     */
    public function setLokfizPkdNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   bool|null  $flag
     */
    public function setLokfizPkdPrzewazajace($flag)
    {
        $this->isMain = (bool)$flag;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizSilosSymbol($code)
    {
        $this->silosSymbol = $code;
    }

    /**
     * @param   string  $code
     */
    public function setPrawPkdKod($code)
    {
        $this->code = $code;
    }

    /**
     * @param   string  $name
     */
    public function setPrawPkdNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   bool|null  $flag
     */
    public function setPrawPkdPrzewazajace($flag)
    {
        $this->isMain = (bool)$flag;
    }

    /**
     * @param   string  $code
     */
    public function setLokprawPkdKod($code)
    {
        $this->code = $code;
    }

    /**
     * @param   string  $name
     */
    public function setLokprawPkdNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   bool|null  $flag
     */
    public function setLokprawPkdPrzewazajace($flag)
    {
        $this->isMain = (bool)$flag;
    }


}