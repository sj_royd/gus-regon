<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

trait FullReport
{
    /**
     * @var string
     */
    public $regon;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $shortName;

    /**
     * @var Report\Address|null
     */
    public $address;

    /**
     * @var Report\Dates|null
     */
    public $dates;

    /**
     * @var Report\Contact|null
     */
    public $contact;

    /**
     * @var Report\Register|null
     */
    public $register;

    /**
     * @var bool
     */
    public $isActive;

}