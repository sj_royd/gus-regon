<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;
use SJRoyd\GUS\RegonApi\Enum\NipStatus;

class Law
{
    use FullReport;

    /**
     * @var string
     */
    public $nip;

    /**
     * @var string
     */
    public $nipStatus;

    /**
     * @var int
     */
    public $localsCount;

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
        $this->contact  = new Report\Contact();
        $this->register = new Report\Register();
    }

    /**
     * @param   string|null  $regon
     */
    public function setPrawRegon9($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string|null  $nip
     */
    public function setPrawNip($nip)
    {
        $this->nip = $nip;
    }

    /**
     * @param   string|null  $nipStatus
     */
    public function setPrawStatusNip($nipStatus)
    {
        $this->nipStatus = NipStatus::parseStatus($nipStatus);
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $shortName
     */
    public function setPrawNazwaSkrocona($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataPowstania($date)
    {
        $this->dates->create = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataRozpoczeciaDzialalnosci($date)
    {
        $this->dates->start = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataWpisuDoRegon($date)
    {
        $this->dates->add = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataZawieszeniaDzialalnosci($date)
    {
        $this->dates->suspend = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataWznowieniaDzialalnosci($date)
    {
        $this->dates->resume = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataZaistnieniaZmiany($date)
    {
        $this->dates->change = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataZakonczeniaDzialalnosci($date)
    {
        $this->dates->close = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataSkresleniaZRegon($date)
    {
        $this->dates->delete = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataOrzeczeniaOUpadlosci($date)
    {
        $this->dates->bankruptcy = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataZakonczeniaPostepowaniaUpadlosciowego($date)
    {
        $this->dates->closeBankruptcyProceeding = $date;
    }


    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzKrajSymbol($code)
    {
        $this->address->countryCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzWojewodztwoSymbol($code)
    {
        $this->address->provinceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzPowiatSymbol($code)
    {
        $this->address->districtCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzGminaSymbol($code)
    {
        $this->address->decodeCommuneCode($code);
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzKodPocztowy($code)
    {
        $this->address->postalCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzMiejscowoscPocztySymbol($code)
    {
        $this->address->postalPlaceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzMiejscowoscSymbol($code)
    {
        $this->address->placeCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawAdSiedzUlicaSymbol($code)
    {
        $this->address->streetCode = $code;
    }

    /**
     * @param   string|null  $number
     */
    public function setPrawAdSiedzNumerNieruchomosci($number)
    {
        $this->address->buildingNumber = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setPrawAdSiedzNumerLokalu($number)
    {
        $this->address->localNumber = $number;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzNietypoweMiejsceLokalizacji($name)
    {
        $this->address->unusualLocation = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzKrajNazwa($name)
    {
        $this->address->countryName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzWojewodztwoNazwa($name)
    {
        $this->address->provinceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzPowiatNazwa($name)
    {
        $this->address->districtName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzGminaNazwa($name)
    {
        $this->address->communeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzMiejscowoscNazwa($name)
    {
        $this->address->placeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzMiejscowoscPocztyNazwa($name)
    {
        $this->address->postalPlaceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawAdSiedzUlicaNazwa($name)
    {
        $this->address->streetName = $name;
    }


    /**
     * @param   string|null  $number
     */
    public function setPrawNumerTelefonu($number)
    {
        $this->contact->phone = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setPrawNumerWewnetrznyTelefonu($number)
    {
        $this->contact->extension = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setPrawNumerFaksu($number)
    {
        $this->contact->fax = $number;
    }

    /**
     * @param   string|null  $email
     */
    public function setPrawAdresEmail($email)
    {
        $this->contact->email = $email;
    }

    /**
     * @param   string|null  $address
     */
    public function setPrawAdresStronyinternetowej($address)
    {
        $this->contact->website = $address;
    }


    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setPrawDataWpisuDoRejestruEwidencji($date)
    {
        $this->register->dateAdd = $date;
    }

    /**
     * @param string|null $number
     */
    public function setPrawNumerWRejestrzeEwidencji($number)
    {
        $this->register->number = $number;
    }

    /**
     * @param string|null $name
     */
    public function setPrawOrganRejestrowySymbol($code)
    {
        $this->register->registrationAuthorityCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setPrawOrganRejestrowyNazwa($name)
    {
        $this->register->registrationAuthorityName = $name;
    }

    /**
     * @param string|null $name
     */
    public function setPrawOrganZalozycielskiSymbol($code)
    {
        $this->register->foundingBodyCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setPrawOrganZalozycielskiNazwa($name)
    {
        $this->register->foundingBodyName = $name;
    }

    /**
     * @var string|null $name
     */
    public function setPrawRodzajRejestruEwidencjiSymbol($name)
    {
        $this->register->typeCode = $name;
    }

    /**
     * @var string|null $name
     */
    public function setPrawRodzajRejestruEwidencjiNazwa($name)
    {
        $this->register->typeName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawFormaFinansowaniaSymbol($code)
    {
        $this->register->financingFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawFormaFinansowaniaNazwa($name)
    {
        $this->register->financingFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawPodstawowaFormaPrawnaSymbol($code)
    {
        $this->register->basicLegalFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawPodstawowaFormaPrawnaNazwa($name)
    {
        $this->register->basicLegalFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawSzczegolnaFormaPrawnaSymbol($code)
    {
        $this->register->specialLegalFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawSzczegolnaFormaPrawnaNazwa($name)
    {
        $this->register->specialLegalFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setPrawFormaWlasnosciSymbol($code)
    {
        $this->register->ownershipFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setPrawFormaWlasnosciNazwa($name)
    {
        $this->register->ownershipFormName = $name;
    }

    /**
     * @param   int|null  $count
     */
    public function setPrawLiczbaJednLokalnych($count)
    {
        $this->localsCount = $count;
    }

}