<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

class PhysicLocalUnit extends PhysicLocal
{

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
        $this->register = new Report\Register();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizCDataWpisuDoRejestruEwidencji($date)
    {
        $this->register->dateAdd = $date;
    }

    /**
     * @param string|null $name
     */
    public function setLokfizCNumerWRejestrzeEwidencji($name)
    {
        $this->register->number = $name;
    }

    /**
     * @param string|null $code
     */
    public function setLokfizCOrganRejestrowySymbol($code)
    {
        $this->register->registrationAuthorityCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setLokfizCOrganRejestrowyNazwa($name)
    {
        $this->register->registrationAuthorityName = $name;
    }

    /**
     * @param string|null $name
     */
    public function setLokfizCRodzajRejestruSymbol($name)
    {
        $this->register->typeCode = $name;
    }

    /**
     * @param string|null $name
     */
    public function setLokfizCRodzajRejestruNazwa($name)
    {
        $this->register->typeName = $name;
    }

    /**
     * @param bool|null $val
     */
    public function setLokfizCNiePodjetoDzialalnosci($val)
    {
        $this->register->noActivity = $val;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizFormaFinansowaniaSymbol($code)
    {
        $this->register->financingFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizFormaFinansowaniaNazwa($name)
    {
        $this->register->financingFormName = $name;
    }

}