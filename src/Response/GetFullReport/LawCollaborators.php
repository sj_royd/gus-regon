<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

class LawCollaborators
{
    /**
     * @var string
     */
    public $regon;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $secondName;

    /**
     * @var string|null
     */
    public $lastName;

    /**
     * @var string|null
     */
    public $companyName;

    /**
     * @param   string  $regon
     */
    public function setWspolscRegonWspolnikSpolki($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string|null  $name
     */
    public function setWspolscImiePierwsze($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $secondName
     */
    public function setWspolscImieDrugie($secondName)
    {
        $this->secondName = $secondName;
    }

    /**
     * @param   string|null  $lastName
     */
    public function setwspolscNazwisko($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param   string|null  $companyName
     */
    public function setWspolscFirmaNazwa($companyName)
    {
        $this->companyName = $companyName;
    }



}