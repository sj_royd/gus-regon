<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;
use SJRoyd\GUS\RegonApi\Enum\NipStatus;

class PhysicGeneral
{
    use FullReport;

    /**
     * @var string
     */
    public $nip;

    /**
     * @var string
     */
    public $nipStatus;

    /**
     * @var string
     */
    public $secondName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var int
     */
    public $localsCount;

    /**
     * @var int
     */
    public $activityCeidg;

    /**
     * @var int
     */
    public $activityAgricultural;

    /**
     * @var int
     */
    public $activityOther;

    /**
     * @var int
     */
    public $activityDeleted;

    public function __construct()
    {
        $this->dates     = new Report\Dates();
        $this->register  = new Report\Register();
        $this->nipStatus = NipStatus::parseStatus('');
    }

    /**
     * @param   string|null  $regon
     */
    public function setFizRegon9($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string|null  $nip
     */
    public function setFizNip($nip)
    {
        $this->nip = $nip;
    }

    /**
     * @param   string|null  $nipStatus
     */
    public function setFizStatusNip($nipStatus)
    {
        $this->nipStatus = NipStatus::parseStatus($nipStatus);
    }

    /**
     * @param   string|null  $name
     */
    public function setFizImie1($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizImie2($name)
    {
        $this->secondName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizNazwisko($name)
    {
        $this->lastName = $name;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizDataWpisuPodmiotuDoRegon($date)
    {
        $this->dates->create = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizDataZaistnieniaZmiany($date)
    {
        $this->dates->change = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizDataSkresleniaPodmiotuZRegon($date)
    {
        $this->dates->delete = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   string|null  $code
     */
    public function setFizFormaFinansowaniaSymbol($code)
    {
        $this->register->financingFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizFormaFinansowaniaNazwa($name)
    {
        $this->register->financingFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizPodstawowaFormaPrawnaSymbol($code)
    {
        $this->register->basicLegalFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizPodstawowaFormaPrawnaNazwa($name)
    {
        $this->register->basicLegalFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizSzczegolnaFormaPrawnaSymbol($code)
    {
        $this->register->specialLegalFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizSzczegolnaFormaPrawnaNazwa($name)
    {
        $this->register->specialLegalFormName = $name;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizFormaWlasnosciSymbol($code)
    {
        $this->register->ownershipFormCode = $code;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizFormaWlasnosciNazwa($name)
    {
        $this->register->ownershipFormName = $name;
    }

    /**
     * @param int|null $count
     */
    public function setFizLiczbaJednLokalnych($count)
    {
        $this->localsCount = $count ?: 0;
    }

    /**
     * @param int|null $count
     */
    public function setFizDzialalnoscCeidg($count)
    {
        $this->activityCeidg = $count ?: 0;
    }

    /**
     * @param int|null $count
     */
    public function setFizDzialalnoscRolnicza($count)
    {
        $this->activityAgricultural = $count ?: 0;
    }

    /**
     * @param int|null $count
     */
    public function setFizDzialalnoscPozostala($count)
    {
        $this->activityOther = $count ?: 0;
    }

    /**
     * @param int|null $count
     */
    public function setFizDzialalnoscSkreslonaDo20141108($count)
    {
        $this->activityDeleted = $count ?: 0;
    }
}