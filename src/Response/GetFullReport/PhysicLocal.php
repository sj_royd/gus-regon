<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

abstract class PhysicLocal
{
    use FullReport;

    /**
     * @var int
     */
    public $silosId;

    /**
     * @var string
     */
    public $silosName;

    /**
     * @param   string  $regon
     */
    public function setLokfizRegon14($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string  $name
     */
    public function setLokfizNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $shortName
     */
    public function setLokfizNazwaSkrocona($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @param   int  $id
     */
    public function setLokFizSilosId($id)
    {
        $this->silosId = $id;
    }

    /**
     * @param   string  $name
     */
    public function setLokFizSilosSymbol($name)
    {
        $this->silosName = $name;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataPowstania($date)
    {
        $this->dates->create = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataRozpoczeciaDzialalnosci($date)
    {
        $this->dates->start = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataWpisuDzialalnosciDoRegon($date)
    {
        $this->dates->add = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataZawieszeniaDzialalnosci($date)
    {
        $this->dates->suspend = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataWznowieniaDzialalnosci($date)
    {
        $this->dates->resume = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataZaistnieniaZmianyDzialalnosci($date)
    {
        $this->dates->change = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataZakonczeniaDzialalnosci($date)
    {
        $this->dates->close = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokfizDataSkresleniaDzialalnosciZRegon($date)
    {
        $this->dates->delete = $date;
    }


    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzKrajSymbol($code)
    {
        $this->address->countryCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzWojewodztwoSymbol($code)
    {
        $this->address->provinceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzPowiatSymbol($code)
    {
        $this->address->districtCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzGminaSymbol($code)
    {
        $this->address->decodeCommuneCode($code);
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzKodPocztowy($code)
    {
        $this->address->postalCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzMiejscowoscPocztySymbol($code)
    {
        $this->address->postalPlaceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzMiejscowoscSymbol($code)
    {
        $this->address->placeCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokfizAdSiedzUlicaSymbol($code)
    {
        $this->address->streetCode = $code;
    }

    /**
     * @param   string|null  $number
     */
    public function setLokfizAdSiedzNumerNieruchomosci($number)
    {
        $this->address->buildingNumber = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setLokfizAdSiedzNumerLokalu($number)
    {
        $this->address->localNumber = $number;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzNietypoweMiejsceLokalizacji($name)
    {
        $this->address->unusualLocation = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzKrajNazwa($name)
    {
        $this->address->countryName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzWojewodztwoNazwa($name)
    {
        $this->address->provinceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzPowiatNazwa($name)
    {
        $this->address->districtName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzGminaNazwa($name)
    {
        $this->address->communeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzMiejscowoscNazwa($name)
    {
        $this->address->placeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzMiejscowoscPocztyNazwa($name)
    {
        $this->address->postalPlaceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokfizAdSiedzUlicaNazwa($name)
    {
        $this->address->streetName = $name;
    }

}