<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\Report\Register;

class LawLocalUnit extends LawLocal
{

    public function __construct()
    {
        parent::__construct();
        $this->register = new Register();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setLokprawDataWpisuDoRejestruEwidencji($date)
    {
        $this->register->dateAdd = $date;
    }

    /**
     * @param string|null $name
     */
    public function setLokprawNumerWRejestrzeEwidencji($name)
    {
        $this->register->number = $name;
    }

    /**
     * @param string|null $code
     */
    public function setLokprawOrganRejestrowySymbol($code)
    {
        $this->register->registrationAuthorityCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setLokprawOrganRejestrowyNazwa($name)
    {
        $this->register->registrationAuthorityName = $name;
    }

    /**
     * @param string|null $name
     */
    public function setLokprawRodzajRejestruEwidencjiSymbol($name)
    {
        $this->register->typeCode = $name;
    }

    /**
     * @param string|null $name
     */
    public function setLokprawRodzajRejestruEwidencjiNazwa($name)
    {
        $this->register->typeName = $name;
    }

    /**
     * @param   string  $code
     */
    public function setLokprawFormaFinansowaniaSymbol($code)
    {
        $this->register->financingFormCode = $code;
    }

    /**
     * @param   string  $name
     */
    public function setLokprawFormaFinansowaniaNazwa($name)
    {
        $this->register->financingFormName = $name;
    }
}