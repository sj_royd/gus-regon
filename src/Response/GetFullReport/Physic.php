<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

abstract class Physic
{
    use FullReport;

    /**
     * @param   string  $regon
     */
    public function setFizRegon9($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string  $name
     */
    public function setFizNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $shortName
     */
    public function setFizNazwaSkrocona($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataPowstania($date)
    {
        $this->dates->create = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataRozpoczeciaDzialalnosci($date)
    {
        $this->dates->start = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataWpisuDzialalnosciDoRegon($date)
    {
        $this->dates->add = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataZawieszeniaDzialalnosci($date)
    {
        $this->dates->suspend = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataWznowieniaDzialalnosci($date)
    {
        $this->dates->resume = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataZaistnieniaZmianyDzialalnosci($date)
    {
        $this->dates->change = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataZakonczeniaDzialalnosci($date)
    {
        $this->dates->close = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataSkresleniaDzialalnosciZRegon($date)
    {
        $this->dates->delete = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataOrzeczeniaOUpadlosci($date)
    {
        $this->dates->bankruptcy = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setFizDataZakonczeniaPostepowaniaUpadlosciowego($date)
    {
        $this->dates->closeBankruptcyProceeding = $date;
    }


    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzKrajSymbol($code)
    {
        $this->address->countryCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzWojewodztwoSymbol($code)
    {
        $this->address->provinceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzPowiatSymbol($code)
    {
        $this->address->districtCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzGminaSymbol($code)
    {
        $this->address->decodeCommuneCode($code);
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzKodPocztowy($code)
    {
        $this->address->postalCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzMiejscowoscPocztySymbol($code)
    {
        $this->address->postalPlaceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzMiejscowoscSymbol($code)
    {
        $this->address->placeCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setFizAdSiedzUlicaSymbol($code)
    {
        $this->address->streetCode = $code;
    }

    /**
     * @param   string|null  $number
     */
    public function setFizAdSiedzNumerNieruchomosci($number)
    {
        $this->address->buildingNumber = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setFizAdSiedzNumerLokalu($number)
    {
        $this->address->localNumber = $number;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzNietypoweMiejsceLokalizacji($name)
    {
        $this->address->unusualLocation = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzKrajNazwa($name)
    {
        $this->address->countryName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzWojewodztwoNazwa($name)
    {
        $this->address->provinceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzPowiatNazwa($name)
    {
        $this->address->districtName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzGminaNazwa($name)
    {
        $this->address->communeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzMiejscowoscNazwa($name)
    {
        $this->address->placeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzMiejscowoscPocztyNazwa($name)
    {
        $this->address->postalPlaceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setFizAdSiedzUlicaNazwa($name)
    {
        $this->address->streetName = $name;
    }



    /**
     * @param   string|null  $number
     */
    public function setFizNumerTelefonu($number)
    {
        $this->contact->phone = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setFizNumerWewnetrznyTelefonu($number)
    {
        $this->contact->extension = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setFizNumerFaksu($number)
    {
        $this->contact->fax = $number;
    }

    /**
     * @param   string|null  $email
     */
    public function setFizAdresEmail($email)
    {
        $this->contact->email = $email;
    }

    /**
     * @param   string|null  $email
     */
    public function setFizAdresEmail2($email)
    {
        $this->contact->email2 = $email;
    }

    /**
     * @param   string|null  $address
     */
    public function setFizAdresStronyinternetowej($address)
    {
        $this->contact->website = $address;
    }

}