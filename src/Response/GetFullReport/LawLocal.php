<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;
use SJRoyd\GUS\RegonApi\Enum\NipStatus;

abstract class LawLocal
{
    use FullReport;
    
    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
    }

    /**
     * @param   string  $regon
     */
    public function setLokprawRegon14($regon)
    {
        $this->regon = $regon;
    }

    /**
     * @param   string  $name
     */
    public function setLokprawNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $shortName
     */
    public function setLokprawNazwaSkrocona($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataPowstania($date)
    {
        $this->dates->create = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataRozpoczeciaDzialalnosci($date)
    {
        $this->dates->start = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataWpisuDzialalnosciDoRegon($date)
    {
        $this->dates->add = $date;
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataZawieszeniaDzialalnosci($date)
    {
        $this->dates->suspend = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataWznowieniaDzialalnosci($date)
    {
        $this->dates->resume = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataZakonczeniaDzialalnosci($date)
    {
        $this->dates->close = $date;
        $this->isActive = $this->dates->prepareActive();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null $date
     */
    public function setLokprawDataSkresleniaZRegon($date)
    {
        $this->dates->delete = $date;
    }


    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzKrajSymbol($code)
    {
        $this->address->countryCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzWojewodztwoSymbol($code)
    {
        $this->address->provinceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzPowiatSymbol($code)
    {
        $this->address->districtCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzGminaSymbol($code)
    {
        $this->address->decodeCommuneCode($code);
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzKodPocztowy($code)
    {
        $this->address->postalCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzMiejscowoscPocztySymbol($code)
    {
        $this->address->postalPlaceCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzMiejscowoscSymbol($code)
    {
        $this->address->placeCode = $code;
    }

    /**
     * @param   string|null  $code
     */
    public function setLokprawAdSiedzUlicaSymbol($code)
    {
        $this->address->streetCode = $code;
    }

    /**
     * @param   string|null  $number
     */
    public function setLokprawAdSiedzNumerNieruchomosci($number)
    {
        $this->address->buildingNumber = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setLokprawAdSiedzNumerLokalu($number)
    {
        $this->address->localNumber = $number;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzNietypoweMiejsceLokalizacji($name)
    {
        $this->address->unusualLocation = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzKrajNazwa($name)
    {
        $this->address->countryName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzWojewodztwoNazwa($name)
    {
        $this->address->provinceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzPowiatNazwa($name)
    {
        $this->address->districtName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzGminaNazwa($name)
    {
        $this->address->communeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzMiejscowoscNazwa($name)
    {
        $this->address->placeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzMiejscowoscPocztyNazwa($name)
    {
        $this->address->postalPlaceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setLokprawAdSiedzUlicaNazwa($name)
    {
        $this->address->streetName = $name;
    }


}