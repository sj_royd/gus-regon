<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport\Report;

class Address extends \SJRoyd\GUS\RegonApi\Response\Address
{
    /**
     * @var string|null
     */
    public $unusualLocation;

    /**
     * @var string|null
     */
    public $countryCode;

    /**
     * @var string|null
     */
    public $provinceCode;

    /**
     * @var string|null
     */
    public $districtCode;

    /**
     * @var string|null
     */
    public $communeCode;

    /**
     * @var int
     */
    public $communeTypeCode;

    /**
     * @var string|null
     */
    public $placeCode;

    /**
     * @var string|null
     */
    public $postalPlaceCode;

    /**
     * @var string|null
     */
    public $streetCode;

    /**
     * @param   string  $code
     */
    public function decodeCommuneCode($code)
    {
        $this->communeCode     = substr($code, 0, 2);
        $this->communeTypeCode = substr($code, 2, 1);
    }

}