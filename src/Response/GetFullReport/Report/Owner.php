<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport\Report;

class Owner
{
    /**
     * @var string|null
     */
    public $firstName;

    /**
     * @var string|null
     */
    public $nameSecond;

    /**
     * @var string|null
     */
    public $lastName;


}
