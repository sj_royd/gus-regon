<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport\Report;

use SJRoyd\GUS\RegonApi\Date;

class Dates
{
    private static $now;

    public function __construct()
    {
        ! self::$now && self::$now = new Date();
    }

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $create;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $start;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $add;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $suspend;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $resume;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $change;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $close;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $delete;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $bankruptcy;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $closeBankruptcyProceeding;

    /**
     * @param   null|Date  $date
     *
     * @return bool
     */
    public function prepareActive()
    {
        $active = true;

        if ($this->start) {
            $active &= ! ! self::$now->diff($this->start)->invert;
        }
        if ($this->resume) {
            $active &= ! ! self::$now->diff($this->resume)->invert;
        }
        if ($this->suspend) {
            $active &= ! self::$now->diff($this->suspend)->invert;
        }
        if ($this->close) {
            $active &= ! self::$now->diff($this->close)->invert;
        }

        return (bool)$active;
    }

}