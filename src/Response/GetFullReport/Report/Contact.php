<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport\Report;

class Contact
{
    /**
     * @var string|null
     */
    public $phone;

    /**
     * @var string|null
     */
    public $extension;

    /**
     * @var string|null
     */
    public $fax;

    /**
     * @var string|null
     */
    public $email;

    /**
     * @var string|null
     */
    public $email2;

    /**
     * @var string|null
     */
    public $website;

}