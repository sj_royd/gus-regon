<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport\Report;

class Register
{
    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */
    public $dateAdd;

    /**
     * @var \SJRoyd\GUS\RegonApi\Date|null
     */

    public $dateDelete;

    /**
     * @var string|null
     */
    public $number;

    /**
     * @var string|null
     */
    public $registrationAuthorityCode;

    /**
     * @var string|null
     */
    public $registrationAuthorityName;

    /**
     * @var string|null
     */
    public $typeCode;

    /**
     * @var string|null
     */
    public $typeName;

    /**
     * @var bool|null
     */
    public $noActivity;

    /**
     * @var string|null
     */
    public $financingFormCode;

    /**
     * @var string|null
     */
    public $financingFormName;

    /**
     * @var string|null
     */
    public $basicLegalFormCode;

    /**
     * @var string|null
     */
    public $basicLegalFormName;

    /**
     * @var string|null
     */
    public $specialLegalFormCode;

    /**
     * @var string|null
     */
    public $specialLegalFormName;

    /**
     * @var string|null
     */
    public $ownershipFormCode;

    /**
     * @var string|null
     */
    public $ownershipFormName;

    /**
     * @var string|null
     */
    public $foundingBodyCode;

    /**
     * @var string|null
     */
    public $foundingBodyName;
}
