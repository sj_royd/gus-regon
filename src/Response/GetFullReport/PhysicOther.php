<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

class PhysicOther extends Physic
{

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
        $this->contact  = new Report\Contact();
        $this->register = new Report\Register();
    }

    /**
     * @param \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizPDataWpisuDoRejestruEwidencji($date)
    {
        $this->register->dateAdd = $date;
    }

    /**
     * @param string|null $number
     */
    public function setFizPNumerWRejestrzeEwidencji($number)
    {
        $this->register->number = $number;
    }

    /**
     * @param string|null $code
     */
    public function setFizPOrganRejestrowySymbol($code)
    {
        $this->register->registrationAuthorityCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setFizPOrganRejestrowyNazwa($name)
    {
        $this->register->registrationAuthorityName = $name;
    }

    /**
     * @param string|null $code
     */
    public function setPizCRodzajRejestruSymbol($code)
    {
        $this->register->typeCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setFizPRodzajRejestruNazwa($name)
    {
        $this->register->typeName = $name;
    }

}