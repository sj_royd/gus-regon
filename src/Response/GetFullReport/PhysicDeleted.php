<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

class PhysicDeleted extends Physic
{

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
        $this->contact  = new Report\Contact();
    }

}