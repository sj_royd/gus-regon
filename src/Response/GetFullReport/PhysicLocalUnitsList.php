<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

class PhysicLocalUnitsList extends PhysicLocal
{

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
    }

}