<?php

namespace SJRoyd\GUS\RegonApi\Response\GetFullReport;

use SJRoyd\GUS\RegonApi\Date;

class PhysicCeidg extends Physic
{

    public function __construct()
    {
        $this->address  = new Report\Address();
        $this->dates    = new Report\Dates();
        $this->contact  = new Report\Contact();
        $this->register = new Report\Register();
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizCDataWpisuDoRejestruEwidencji($date)
    {
        $this->register->dateAdd = $date;
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     */
    public function setFizCDataSkresleniaZRejestruEwidencji($date)
    {
        $this->register->dateDelete = $date;
    }

    /**
     * @param string|null $number
     */
    public function setFizCNumerWRejestrzeEwidencji($number)
    {
        $this->register->number = $number;
    }

    /**
     * @param string|null $code
     */
    public function setFizCOrganRejestrowySymbol($code)
    {
        $this->register->registrationAuthorityCode = $code;
    }

    /**
     * @param string|null $name
     */
    public function setFizCOrganRejestrowyNazwa($name)
    {
        $this->register->registrationAuthorityName = $name;
    }

    /**
     * @param string|null $name
     */
    public function setFizCRodzajRejestruSymbol($name)
    {
        $this->register->typeCode = $name;
    }

    /**
     * @param string|null $name
     */
    public function setFizCRodzajRejestruNazwa($name)
    {
        $this->register->typeName = $name;
    }

    /**
     * @param bool|null $val
     */
    public function setFizCNiePodjetoDzialalnosci($val)
    {
        $this->register->noActivity = $val;
    }

}