<?php

namespace SJRoyd\GUS\RegonApi\Response\SearchUnits;

use SJRoyd\GUS\RegonApi\BIRService;
use SJRoyd\GUS\RegonApi\Date;
use SJRoyd\GUS\RegonApi\Enum\NipStatus;
use SJRoyd\GUS\RegonApi\Enum\ReportType;
use SJRoyd\GUS\RegonApi\Enum\Silos;
use SJRoyd\GUS\RegonApi\Enum\UnitType;
use SJRoyd\GUS\RegonApi\Exception;
use SJRoyd\GUS\RegonApi\Response\Address;

class Unit
{
    /**
     * @var string
     */
    public $regon;

    /**
     * @var string
     */
    public $nip;

    /**
     * @var string
     */
    public $nipStatus;

    /**
     * @var string
     */
    public $name;

    /**
     * @var Address
     */
    public $address;

    /**
     * @var string
     */
    public $type;

    /**
     * @var int
     */
    public $silosId;

    /**
     * @var string
     */
    public $activityType;

    /**
     * @var Date|null
     */
    public $closeDate;

    /**
     * @var bool
     */
    public $isActive;

    /**
     * @var array
     */
    public $reports = [];

    public function __construct()
    {
        $this->address   = new Address();
        $this->nipStatus = NipStatus::parseStatus('');
    }

    /**
     * @param   string|null  $Regon
     */
    public function setRegon($Regon)
    {
        $this->regon = $Regon;
    }

    /**
     * @param   string|null  $nip
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
    }

    /**
     * @param   string|null  $nipStatus
     */
    public function setStatusNip($nipStatus)
    {
        $this->nipStatus = NipStatus::parseStatus($nipStatus);
    }

    /**
     * @param   string|null  $name
     */
    public function setNazwa($name)
    {
        $this->name = $name;
    }

    /**
     * @param   string|null  $type
     */
    public function setTyp($type)
    {
        $this->type = $type;
    }

    /**
     * @param   int|null  $silosId
     */
    public function setSilosId($silosId)
    {
        $this->silosId      = $silosId;
        $this->activityType = Silos::getSilosTypeName($silosId);
    }

    /**
     * @param   \SJRoyd\GUS\RegonApi\Date|null  $date
     *
     * @throws \Exception
     */
    public function setDataZakonczeniaDzialalnosci($date)
    {
        $this->closeDate = $date;
        $this->isActive = $date ? !((new \DateTime())->diff($date)->invert) : true;
    }

    /**
     * @param   string|null  $name
     */
    public function setWojewodztwo($name)
    {
        $this->address->provinceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setPowiat($name)
    {
        $this->address->districtName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setGmina($name)
    {
        $this->address->communeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setMiejscowosc($name)
    {
        $this->address->placeName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setKodPocztowy($name)
    {
        $this->address->postalCode = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setMiejscowoscPoczty($name)
    {
        $this->address->postalPlaceName = $name;
    }

    /**
     * @param   string|null  $name
     */
    public function setUlica($name)
    {
        $this->address->streetName = $name;
    }

    /**
     * @param   string|null  $number
     */
    public function setNrNieruchomosci($number)
    {
        $this->address->buildingNumber = $number;
    }

    /**
     * @param   string|null  $number
     */
    public function setNrLokalu($number)
    {
        $this->address->localNumber = $number;
    }

    /**
     * @return array
     * @throws Exception\NotFoundException
     * @throws Exception\PkdException
     * @throws Exception\ReportException
     * @throws \JsonMapper_Exception
     * @throws \Exception
     */
    public function getReports()
    {
        $api = BIRService::$self;
        if ( ! $this->reports) {
            $reportsList = ReportType::getReports($this->type, $this->silosId);
            foreach ($reportsList as $index => $reportName) {
                if (
                    isset($this->reports['general'])
                    && ! $this->reports['general']->localsCount
                    && (
                        $reportName == ReportType::PHYSIC_LOCALS_LIST
                        || $reportName == ReportType::LAW_LOCALS_LIST
                    )
                ) {
                    continue;
                }
                try {
                    $reports = $api->getFullReport($this->regon, $reportName);
                    switch ($index) {
                        case 'locals':
                        case 'pkd':
                            $r = [];
                            foreach ($reports as $key => $report) {
                                if (
                                    $this->type != UnitType::PHYSIC
                                    || $this->silosId == $report->silosId
                                ) {
                                    $r[] = $report;
                                }
                            }
                            $reports = $r;
                            break;
                    }
                    $this->reports[$index] = $reports;
                } catch (\Exception $e) {
                    switch ($index) {
                        case 'collaborators':
                            break;
                        default:
                            throw $e;
                    }
                }
            }
        }

        return $this->reports;
    }

}