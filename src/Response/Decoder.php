<?php

namespace SJRoyd\GUS\RegonApi\Response;

use SJRoyd\GUS\RegonApi\Exception\CivilLawPartnershipException;
use SJRoyd\GUS\RegonApi\Exception\CurrentDateException;
use SJRoyd\GUS\RegonApi\Exception\FutureDateException;
use SJRoyd\GUS\RegonApi\Exception\InvalidKeyException;
use SJRoyd\GUS\RegonApi\Exception\NotFoundException;
use SJRoyd\GUS\RegonApi\Exception\PastDateException;
use SJRoyd\GUS\RegonApi\Exception\PkdException;
use SJRoyd\GUS\RegonApi\Exception\ReportException;
use SJRoyd\GUS\RegonApi\Exception\ServerOverflowException;
use SJRoyd\HTTPService\JsonXMLElement;

class Decoder
{

    /**
     * @param   string  $data
     *
     * @return \stdClass
     * @throws InvalidKeyException
     */
    protected static function decode($data)
    {
        if ( ! $data) {
            throw new InvalidKeyException();
        }
        $xml = new JsonXMLElement($data);

        return json_decode(json_encode($xml));
    }

    /**
     * @param   string  $data
     *
     * @return \stdClass[]
     * @throws NotFoundException
     */
    public static function search($data)
    {
        $data = self::decode($data);
        if (isset($data->dane->ErrorCode)) {
            $err = $data->dane;
            $details = [];
            isset($err->Krs)        && $details = ['KRS'    => $err->Krs];
            isset($err->Krsy)       && $details = ['KRSes'  => $err->Krsy];
            isset($err->Nip)        && $details = ['NIP'    => $err->Nip];
            isset($err->Nipy)       && $details = ['NIPs'   => $err->Nipy];
            isset($err->Regon)      && $details = ['REGON'  => $err->Regon];
            isset($err->Regony9zn)  && $details = ['REGONs' => $err->Regony9zn];
            isset($err->Regony14zn) && $details = ['REGONs' => $err->Regony14zn];

            throw new NotFoundException(
                $err->ErrorMessageEn,
                $err->ErrorCode,
                $details
            );
        }
        if (!is_array($data->dane)) {
            $data->dane = [$data->dane];
        }

        return $data->dane;
    }

    /**
     * @param         $data
     *
     * @param   bool  $forceArray
     *
     * @return \stdClass
     * @throws CivilLawPartnershipException
     * @throws NotFoundException
     * @throws PkdException
     * @throws ReportException
     */
    public static function report($data, $forceArray = false)
    {
        $data = self::decode($data);
        if (isset($data->dane->ErrorCode)) {
            $err = $data->dane;
            switch ($err->ErrorCode) {
                case 4:
                    throw new NotFoundException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode,
                        [
                            'REGON' => $err->pRegon,
                            'unit type' => $err->Typ_podmiotu,
                            'report type' => $err->Raport
                        ]);
                case 5:
                    throw new ReportException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode,
                        [
                            'REGON' => $err->pRegon,
                            'unit type' => $err->Typ_podmiotu,
                            'report type' => $err->Raport
                        ]);
                case 11:
                    throw new PkdException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode,
                        [
                            'REGON' => $err->pRegon,
                            'unit type' => $err->Typ_podmiotu,
                            'report type' => $err->Raport
                        ]);
                case 21:
                    throw new CivilLawPartnershipException(
                        $err->DatabaseSearchResultEn,
                        $err->ErrorCode,
                        [
                            'REGON' => $err->pRegon
                        ]);
                case 22:
                    throw new CivilLawPartnershipException(
                        $err->DatabaseSearchResultEn,
                        $err->ErrorCode,
                        [
                            'REGON' => $err->pRegon
                        ]);
            }
        }
        if ($forceArray && !is_array($data->dane)) {
            $data->dane = [$data->dane];
        }

        return $data;
    }

    /**
     * @param $data
     *
     * @return array
     * @throws CurrentDateException
     * @throws FutureDateException
     * @throws NotFoundException
     * @throws PastDateException
     * @throws ReportException
     * @throws ServerOverflowException
     */
    public static function compact($data)
    {
        $data = self::decode($data);
        if (isset($data->dane->ErrorCode)) {
            $err = $data->dane;
            switch ($err->ErrorCode) {
                case 4:
                    throw new NotFoundException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
                case 101:
                    throw new ServerOverflowException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
                case 102:
                    throw new CurrentDateException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
                case 103:
                    throw new FutureDateException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
                case 104:
                    throw new PastDateException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
                case 105:
                    throw new ReportException(
                        $err->ErrorMessageEn,
                        $err->ErrorCode);
            }
        }
        if (!is_array($data->dane)) {
            $data->dane = [$data->dane];
        }

        return array_column($data->dane, 'regon');
    }

}