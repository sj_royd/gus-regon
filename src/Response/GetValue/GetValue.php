<?php

namespace SJRoyd\GUS\RegonApi\Response\GetValue;

use SJRoyd\GUS\RegonApi\Enum\Value;

class GetValue
{
    const SESSION_NOT_EXISTS = 0;
    const SESSION_EXISTS = 1;

    const SERVICE_UNAVAILABLE = 0;
    const SERVICE_AVAILABLE = 1;
    const SERVICE_BREAK = 2;

    const MESSAGE_OK = 0;
    const MESSAGE_TOO_MANY_IDENTIFIERS = 2;
    const MESSAGE_UNITS_NOT_FOUND = 4;
    const MESSAGE_INVALID_REPORT_NAME = 5;
    const MESSAGE_NO_SESSION= 7;

    public $result;

    private $paramName;

    private $map = [
        Value::SESSION_STATUS => [
            self::SESSION_NOT_EXISTS => 'Session not exists',
            self::SESSION_EXISTS => 'Session exists'
        ],
        Value::SERVICE_STATUS => [
            self::SERVICE_UNAVAILABLE => 'Service unavailable',
            self::SERVICE_AVAILABLE => 'Service available',
            self::SERVICE_BREAK => 'Technical break',
        ],
        Value::MESSAGE_CODE => [
            self::MESSAGE_OK => 'OK',
            self::MESSAGE_TOO_MANY_IDENTIFIERS => 'Too many identifiers',
            self::MESSAGE_UNITS_NOT_FOUND => 'Units not found',
            self::MESSAGE_INVALID_REPORT_NAME => 'Invalid report name',
            self::MESSAGE_NO_SESSION => 'No session',
        ]
    ];

    /**
     * @param string $result
     */
    public function setGetValueResult($result)
    {
        $this->result = $result;
    }

    /**
     * @param string $name
     */
    public function setParam($name)
    {
        $this->paramName = $name;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        if(array_key_exists($this->paramName, $this->map)){
            return $this->map[$this->paramName][$this->result];
        }
        return $this->result;
    }
}