<?php

namespace SJRoyd\GUS\RegonApi\Response\Login;

class Login
{

    /**
     * @var string|null
     */
    public $sid;

    /**
     * @param   string|null  $sid
     */
    public function setZalogujResult($sid)
    {
        $this->sid = $sid;
    }

}