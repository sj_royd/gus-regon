<?php

namespace SJRoyd\GUS\RegonApi\Response;

class Address
{
    /**
     * @var string
     */
    public $countryName;

    /**
     * @var string
     */
    public $provinceName;

    /**
     * @var string
     */
    public $districtName;

    /**
     * @var string
     */
    public $communeName;

    /**
     * @var string
     */
    public $placeName;

    /**
     * @var string
     */
    public $postalCode;

    /**
     * @var string
     */
    public $postalPlaceName;

    /**
     * @var string
     */
    public $streetName;

    /**
     * @var string
     */
    public $buildingNumber;

    /**
     * @var string
     */
    public $localNumber;


}