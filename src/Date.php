<?php

namespace SJRoyd\GUS\RegonApi;

class Date extends \DateTime implements \JsonSerializable
{
    private static $format = 'Y-m-d';

    /**
     * @var self
     */
    public static $now;

    /**
     * @param string $format
     */
    public static function setFormat($format)
    {
        self::$format = $format;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $this->format(self::$format);
    }
}