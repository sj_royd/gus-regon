<?php

namespace SJRoyd\GUS\RegonApi;

class Client extends \SoapClient
{
    /**
     * @var string
     */
    private $headerNs = 'http://www.w3.org/2005/08/addressing';

    private $actionUrl   = 'http://CIS/BIR/PUBL/2014/07/IUslugaBIRzewnPubl';
    private $getValueUrl = 'http://CIS/BIR/2014/07/IUslugaBIR';

    /**
     * @var resource
     */
    private $context;

    public function __construct($wsdl, array $options = null)
    {
        $this->context = stream_context_create();
        $options['stream_context'] = $this->context;
        parent::__construct($wsdl, $options);
    }

    /**
     * @param   string  $function_name
     * @param   array   $arguments
     * @param   null    $options
     * @param   null    $input_headers
     * @param   null    $output_headers
     *
     * @return mixed
     */
    public function __soapCall(
        $function_name,
        $arguments,
        $options = null,
        $input_headers = null,
        &$output_headers = null
    ) {
        $input_headers = $this->getRequestHeaders($function_name);
        return parent::__soapCall($function_name, [$arguments], $options, $input_headers,
            $output_headers);
    }

    /**
     * @param   string  $request
     * @param   string  $location
     * @param   string  $action
     * @param   int     $version
     * @param   int     $one_way
     *
     * @return string
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $response = parent::__doRequest($request, $location, $action, $version, $one_way);

        return stristr(stristr($response, '<s:'), '</s:Envelope>', true) . '</s:Envelope>';;
    }

    /**
     * @param $sid
     */
    public function setSidHeader($sid)
    {
        stream_context_set_option($this->context, [
            'http' => ['header' => "sid: {$sid}"]
        ]);
    }

    /**
     * @param $action
     *
     * @return array
     */
    private function getRequestHeaders($action)
    {
        $actionUrl = $action =='GetValue' ? $this->getValueUrl : $this->actionUrl;
        return [
            new \SoapHeader($this->headerNs, 'Action', $actionUrl . '/' . $action),
            new \SoapHeader($this->headerNs, 'To', $this->location),
        ];
    }
}