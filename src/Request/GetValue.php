<?php

namespace SJRoyd\GUS\RegonApi\Request;

class GetValue
{
    public $pNazwaParametru;

    public function __construct($name)
    {
        $this->pNazwaParametru = $name;
    }
}