<?php

namespace SJRoyd\GUS\RegonApi\Request;

use SJRoyd\GUS\RegonApi\Enum\SearchType;
use SJRoyd\GUS\RegonApi\Exception\InvalidSearchTypeException;
use SJRoyd\GUS\RegonApi\Exception\NumberMissingException;
use SJRoyd\GUS\RegonApi\Exception\TooManyNumbersException;

class SearchData
{
    const MAX_NUMBERS_COUNT = 20;

    public $pParametryWyszukiwania;

    /**
     * SearchData constructor.
     *
     * @param $type
     * @param $numbers
     *
     * @throws NumberMissingException
     * @throws InvalidSearchTypeException
     * @throws TooManyNumbersException
     */
    public function __construct($type, $numbers)
    {
        $this->pParametryWyszukiwania = new SearchParams();
        switch($type){
            case SearchType::KRS:
                $this->pParametryWyszukiwania->Krs = $this->prepareNumbers($numbers, 1);
                break;
            case SearchType::KRSES:
                $this->pParametryWyszukiwania->Krsy = $this->prepareNumbers($numbers);
                break;
            case SearchType::NIP:
                $this->pParametryWyszukiwania->Nip = $this->prepareNumbers($numbers, 1);
                break;
            case SearchType::NIPS:
                $this->pParametryWyszukiwania->Nipy = $this->prepareNumbers($numbers);
                break;
            case SearchType::REGON:
                $this->pParametryWyszukiwania->Regon = $this->prepareNumbers($numbers, 1);
                break;
            case SearchType::REGONS9:
                $this->pParametryWyszukiwania->Regony9zn = $this->prepareNumbers($numbers);
                break;
            case SearchType::REGONS14:
                $this->pParametryWyszukiwania->Regony14zn = $this->prepareNumbers($numbers);
                break;
            default:
                throw new InvalidSearchTypeException($type);
        }
    }

    /**
     * @param   mixed  $numbers
     * @param   bool   $first
     *
     * @return mixed|string
     * @throws NumberMissingException
     * @throws TooManyNumbersException
     */
    private function prepareNumbers($numbers, $first = false)
    {
        if(is_string($numbers)) {
            $numbers = trim($numbers);
            $numbers = preg_replace('~\s+~', ' ', $numbers);
            $numbers = strtr($numbers, [
                ',' => ' ',
                '.' => ' ',
                ';' => ' ',
            ]);
            $numbers = explode(' ', $numbers);
        }
        if ( ! $numbers) {
            throw new NumberMissingException();
        }
        if (count($numbers) > self::MAX_NUMBERS_COUNT) {
            throw new TooManyNumbersException();
        }

        return $first ? $numbers[0] : implode(' ', $numbers);
    }
}