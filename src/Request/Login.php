<?php

namespace SJRoyd\GUS\RegonApi\Request;

use SJRoyd\GUS\RegonApi\Exception\KeyMissingException;

class Login
{
    public $pKluczUzytkownika;

    public function __construct($key = null, $test = false)
    {
        if($test){
            $key = 'abcde12345abcde12345';
        }
        if(!$key){
            throw new KeyMissingException();
        }
        $this->pKluczUzytkownika = $key;
    }
}