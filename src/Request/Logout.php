<?php

namespace SJRoyd\GUS\RegonApi\Request;

class Logout
{
    public $pIdentyfikatorSesji;

    public function __construct($sid)
    {
        $this->pIdentyfikatorSesji = $sid;
    }
}