<?php

namespace SJRoyd\GUS\RegonApi\Request;

use SJRoyd\GUS\RegonApi\Date;

class CompactData
{
    /**
     * @var string
     */
    public $pDataRaportu;

    /**
     * @var string
     */
    public $pNazwaRaportu;

    /**
     * @param   mixed   $date
     * @param   string  $reportName
     *
     * @throws \Exception
     */
    public function __construct($date, $reportName)
    {
        if(is_string($date)){
            $date = new Date($date);
        }

        $this->pDataRaportu  = $date->jsonSerialize();
        $this->pNazwaRaportu = $reportName;
    }

}