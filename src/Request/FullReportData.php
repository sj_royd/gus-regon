<?php

namespace SJRoyd\GUS\RegonApi\Request;

class FullReportData
{
    /**
     * @var string
     */
    public $pRegon;

    /**
     * @var string
     */
    public $pNazwaRaportu;

    /**
     * FullReportData constructor.
     *
     * @param string  $regon
     * @param string  $reportName
     */
    public function __construct($regon, $reportName)
    {
        $this->pRegon        = $regon;
        $this->pNazwaRaportu = $reportName;
    }

}