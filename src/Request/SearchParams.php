<?php

namespace SJRoyd\GUS\RegonApi\Request;

class SearchParams
{
    /**
     * @var string|null
     */
    public $Krs;

    /**
     * @var string|null
     */
    public $Krsy;

    /**
     * @var string|null
     */
    public $Nip;

    /**
     * @var string|null
     */
    public $Nipy;

    /**
     * @var string|null
     */
    public $Regon;

    /**
     * @var string|null
     */
    public $Regony14zn;

    /**
     * @var string|null
     */
    public $Regony9zn;

}