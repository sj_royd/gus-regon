<?php

namespace SJRoyd\GUS\RegonApi;

use SJRoyd\GUS\RegonApi\Enum\ReportType;
use SJRoyd\GUS\RegonApi\Response\Decoder;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\Law;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\LawLocalUnit;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\LawLocalUnitsList;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicAgriculture;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicCeidg;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicDeleted;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicGeneral;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicLocalUnit;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicLocalUnitsList;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PhysicOther;
use SJRoyd\GUS\RegonApi\Response\GetFullReport\PKD;
use SJRoyd\HTTPService\SoapRequest;

class BIRService extends SoapRequest
{
    private $paths = [
        'prod' => [
            'wsdl' => 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/wsdl/UslugaBIRzewnPubl-ver11-prod.wsdl',
            'loc' => 'https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc'
        ],
        'test' => [
            'wsdl' => 'https://wyszukiwarkaregontest.stat.gov.pl/wsBIR/wsdl/UslugaBIRzewnPubl-ver11-test.wsdl',
            'loc' => 'https://wyszukiwarkaregontest.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc'
        ]
    ];

    protected $sid;

    /**
     * @var Client
     */
    protected $client;

    private $loc;

    public static $self;

    public function __construct($test = false, $debug = false)
    {
        $key = $test ? 'test' : 'prod';
        $this->ws_path = $this->paths[$key]['wsdl'];
        $this->loc = $this->paths[$key]['loc'];
        parent::__construct($test, $debug, Client::class);
        $this->client->__setLocation($this->loc);
        self::$self = $this;
        Date::$now = new Date();
    }

    public function __destruct()
    {
        $this->logout();
    }

    /**
     * @param   string  $msg
     * @param   int     $code
     *
     * @throws \Exception
     */
    protected function error($msg, $code = null)
    {
        throw new \Exception($msg, $code);
    }

    /**
     * @param   null  $key
     *
     * @throws Exception\KeyMissingException
     * @throws Exception\InvalidKeyException
     */
    public function login($key = null)
    {
        /** @var Response\Login\Login $response */
        $response = $this->call(
            'Zaloguj',
            new Request\Login($key, $this->test),
            [
                200 => Response\Login\Login::class
            ]
        );
        if(!$response->sid){
            throw new Exception\InvalidKeyException();
        }
        $this->sid = $response->sid;
    }

    /**
     *
     */
    public function logout()
    {
        $response = $this->call(
            'Wyloguj',
            new Request\Logout($this->sid)
        );
    }

    /**
     * @param   string  $type
     * @param   mixed   $numbers
     *
     * @return Response\SearchUnits\Unit[]
     * @throws Exception\InvalidSearchTypeException
     * @throws Exception\NotFoundException
     * @throws Exception\NumberMissingException
     * @throws Exception\TooManyNumbersException
     * @throws \JsonMapper_Exception
     */
    public function search($type, $numbers)
    {
        $this->client->setSidHeader($this->sid);
        $response = $this->call(
            'DaneSzukajPodmioty',
            new Request\SearchData($type, $numbers)
        );
        $data = Decoder::search($response->DaneSzukajPodmiotyResult);
        return self::parseResponse(
            $this->responseStatusCode,
            null,
            $data,
            [
                200 => Response\SearchUnits\Unit::class
            ]
        );
    }

    /**
     * @param   string  $regon
     * @param   string  $reportName
     *
     * @return PhysicGeneral|PhysicCeidg|PhysicAgriculture|PhysicOther|PhysicDeleted|PhysicLocalUnitsList|PhysicLocalUnit|Law|LawLocalUnitsList|LawLocalUnit|PKD
     * @throws Exception\CivilLawPartnershipException
     * @throws Exception\NotFoundException
     * @throws Exception\PkdException
     * @throws Exception\ReportException
     * @throws \JsonMapper_Exception
     */
    public function getFullReport($regon, $reportName)
    {
        $this->client->setSidHeader($this->sid);
        $response = $this->call(
            'DanePobierzPelnyRaport',
            new Request\FullReportData($regon, $reportName)
        );
        $asArray = false;
        if(in_array($reportName, [
            ReportType::PHYSIC_PKD,
            ReportType::PHYSIC_LOCAL_PKD,
            ReportType::LAW_PKD,
            ReportType::LAW_LOCAL_PKD,
            ReportType::PHYSIC_LOCALS_LIST,
            ReportType::LAW_LOCALS_LIST,
        ])){
            $asArray = true;
        }
        $data = Decoder::report($response->DanePobierzPelnyRaportResult, $asArray);
//        print_r($data);
        return self::parseResponse(
            $this->responseStatusCode,
            null,
            $data->dane,
            [
                200 => ReportType::getInstance($reportName)
            ]
        );
    }

    /**
     * @param $date
     * @param $reportName
     *
     * @return array
     * @throws Exception\CurrentDateException
     * @throws Exception\FutureDateException
     * @throws Exception\NotFoundException
     * @throws Exception\PastDateException
     * @throws Exception\ReportException
     * @throws Exception\ServerOverflowException
     * @throws \Exception
     */
    public function getCompactReport($date, $reportName)
    {
        $this->client->setSidHeader($this->sid);
        $response = $this->call(
            'DanePobierzRaportZbiorczy',
            new Request\CompactData($date, $reportName)
        );

        return Decoder::compact($response->DanePobierzRaportZbiorczyResult);
    }

    /**
     * @param $paramName
     *
     * @return Response\GetValue\GetValue
     */
    public function getValue($paramName)
    {
        $this->client->setSidHeader($this->sid);
        $response = $this->call(
            'GetValue',
            new Request\GetValue($paramName),
            [
                200 => Response\GetValue\GetValue::class
            ]
        );
        $response->setParam($paramName);

        return $response;
    }
}