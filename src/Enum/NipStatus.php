<?php

namespace SJRoyd\GUS\RegonApi\Enum;

class NipStatus
{
    const ACTIVE = 'ACTIVE';

    const ANNULLED = 'ANNULLED';

    const CANCELED = 'CANCELED';

    private static $map
        = [
            ''             => self::ACTIVE,
            'Uchylony'     => self::ANNULLED,
            'Unieważniony' => self::CANCELED,
        ];

    /**
     * @param   string  $status
     *
     * @return string
     */
    public static function parseStatus($status)
    {
        return self::$map[ucfirst($status)];
    }
}