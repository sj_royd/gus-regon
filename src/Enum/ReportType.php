<?php

namespace SJRoyd\GUS\RegonApi\Enum;

use SJRoyd\GUS\RegonApi\Response\GetFullReport;

class ReportType
{
    const PHYSIC_GENERAL       = 'BIR11OsFizycznaDaneOgolne';
    const PHYSIC_CEIDG         = 'BIR11OsFizycznaDzialalnoscCeidg';
    const PHYSIC_AGRICULTURAL  = 'BIR11OsFizycznaDzialalnoscRolnicza';
    const PHYSIC_OTHER         = 'BIR11OsFizycznaDzialalnoscPozostala';
    const PHYSIC_DELETED       = 'BIR11OsFizycznaDzialalnoscSkreslona';
    const PHYSIC_PKD           = 'BIR11OsFizycznaPkd';
    const PHYSIC_LOCALS_LIST   = 'BIR11OsFizycznaListaJednLokalnych';
    const PHYSIC_LOCAL         = 'BIR11JednLokalnaOsFizycznej';
    const PHYSIC_LOCAL_PKD     = 'BIR11JednLokalnaOsFizycznejPkd';
    const LAW                  = 'BIR11OsPrawna';
    const LAW_PKD              = 'BIR11OsPrawnaPkd';
    const LAW_LOCALS_LIST      = 'BIR11OsPrawnaListaJednLokalnych';
    const LAW_LOCAL            = 'BIR11JednLokalnaOsPrawnej';
    const LAW_LOCAL_PKD        = 'BIR11JednLokalnaOsPrawnejPkd';
    const LAW_SC_COLLABORATORS = 'BIR11OsPrawnaSpCywilnaWspolnicy';
    const LAW_UNIT_TYPE        = 'BIR11TypPodmiotu';

    private static $reports = [
        UnitType::PHYSIC       => [
            Silos::CEIDG   => [
                'general' => self::PHYSIC_GENERAL,
                'ceidg'   => self::PHYSIC_CEIDG,
                'pkd'     => self::PHYSIC_PKD,
                'locals'  => self::PHYSIC_LOCALS_LIST,
            ],
            Silos::AGRICULTURAL => [
                'general'      => self::PHYSIC_GENERAL,
                'agricultural' => self::PHYSIC_AGRICULTURAL,
                'pkd'          => self::PHYSIC_PKD,
                'locals'       => self::PHYSIC_LOCALS_LIST,
            ],
            Silos::OTHER   => [
                'general' => self::PHYSIC_GENERAL,
                'other'   => self::PHYSIC_OTHER,
                'pkd'     => self::PHYSIC_PKD,
                'locals'  => self::PHYSIC_LOCALS_LIST,
            ],
            Silos::DELETED => [
                'deleted' => self::PHYSIC_DELETED,
            ],
        ],
        UnitType::LOCAL_PHYSIC => [
            Silos::CEIDG => [
                'general' => self::PHYSIC_LOCAL,
                'pkd'     => self::PHYSIC_LOCAL_PKD,
            ],
            Silos::AGRICULTURAL => [
                'general' => self::PHYSIC_LOCAL,
                'pkd'     => self::PHYSIC_LOCAL_PKD,
            ],
            Silos::OTHER => [
                'general' => self::PHYSIC_LOCAL,
                'pkd'     => self::PHYSIC_LOCAL_PKD,
            ],
        ],
        UnitType::LAW          => [
            Silos::LAW => [
                'general'       => self::LAW,
                'pkd'           => self::LAW_PKD,
                'locals'        => self::LAW_LOCALS_LIST,
                'collaborators' => self::LAW_SC_COLLABORATORS,
//                'types'         => self::LAW_UNIT_TYPE,
            ]
        ],
        UnitType::LOCAL_LAW    => [
            Silos::LAW => [
                'general' => self::LAW_LOCAL,
                'pkd'     => self::LAW_LOCAL_PKD,
            ]
        ]
    ];

    private static $map = [
        self::PHYSIC_GENERAL       => GetFullReport\PhysicGeneral::class,
        self::PHYSIC_CEIDG         => GetFullReport\PhysicCeidg::class,
        self::PHYSIC_AGRICULTURAL  => GetFullReport\PhysicAgriculture::class,
        self::PHYSIC_OTHER         => GetFullReport\PhysicOther::class,
        self::PHYSIC_DELETED       => GetFullReport\PhysicDeleted::class,
        self::PHYSIC_PKD           => GetFullReport\PKD::class,
        self::PHYSIC_LOCALS_LIST   => GetFullReport\PhysicLocalUnitsList::class,
        self::PHYSIC_LOCAL         => GetFullReport\PhysicLocalUnit::class,
        self::PHYSIC_LOCAL_PKD     => GetFullReport\PKD::class,
        self::LAW                  => GetFullReport\Law::class,
        self::LAW_PKD              => GetFullReport\PKD::class,
        self::LAW_LOCALS_LIST      => GetFullReport\LawLocalUnitsList::class,
        self::LAW_LOCAL            => GetFullReport\LawLocalUnit::class,
        self::LAW_LOCAL_PKD        => GetFullReport\PKD::class,
        self::LAW_SC_COLLABORATORS => GetFullReport\LawCollaborators::class,
    ];

    /**
     * @param   string  $unitType
     * @param   string  $silosId
     *
     * @return array
     */
    public static function getReports($unitType, $silosId)
    {
        return self::$reports[$unitType][$silosId];
    }

    /**
     * @param   string  $reportName
     *
     * @return string
     */
    public static function getInstance($reportName)
    {
        return self::$map[$reportName];
    }
}