<?php

namespace SJRoyd\GUS\RegonApi\Enum;

class SearchType
{
    const KRS      = 'Krs';
    const KRSES    = 'Krsy';
    const NIP      = 'Nip';
    const NIPS     = 'Nipy';
    const REGON    = 'Regon';
    const REGONS9  = 'Regony9zn';
    const REGONS14 = 'Regony14zn';
}