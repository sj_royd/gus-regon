<?php


namespace SJRoyd\GUS\RegonApi\Enum;


class Value
{
    const DATA_STATUS    = 'StanDanych';
    const MESSAGE_CODE   = 'KomunikatKod';
    const MESSAGE        = 'KomunikatTresc';
    const SESSION_STATUS = 'StatusSesji';
    const SERVICE_STATUS = 'StatusUslugi';
    const SERVICE_MSG    = 'KomunikatUslugi';
}