<?php

namespace SJRoyd\GUS\RegonApi\Enum;

class Silos
{
    const CEIDG        = 1;
    const AGRICULTURAL = 2;
    const OTHER        = 3;
    const DELETED      = 4;
    const LAW          = 6;

    private static $types = [
        self::CEIDG        => 'ceidg',
        self::AGRICULTURAL => 'agricultural',
        self::OTHER        => 'other',
        self::DELETED      => 'deleted',
        self::LAW          => 'law',
    ];

    /**
     * @param $id
     *
     * @return string
     */
    public static function getSilosTypeName($id)
    {
        return self::$types[$id];
    }
}