<?php

namespace SJRoyd\GUS\RegonApi\Enum;

class UnitType
{
    const PHYSIC = 'F';
    const LOCAL_PHYSIC = 'LF';
    const LAW = 'P';
    const LOCAL_LAW = 'LP';
}