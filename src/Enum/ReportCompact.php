<?php

namespace SJRoyd\GUS\RegonApi\Enum;

class ReportCompact
{
    const NEW_UNITS      = 'BIR11NowePodmiotyPrawneOrazDzialalnosciOsFizycznych';
    const UPDATED_UNITS  = 'BIR11AktualizowanePodmiotyPrawneOrazDzialalnosciOsFizycznych';
    const DELETED_UNITS  = 'BIR11SkreslonePodmiotyPrawneOrazDzialalnosciOsFizycznych';
    const NEW_LOCALS     = 'BIR11NoweJednostkiLokalne';
    const UPDATED_LOCALS = 'BIR11AktualizowaneJednostkiLokalne';
    const DELETED_LOCALS = 'BIR11SkresloneJednostkiLokalne';
}