<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class InvalidSearchTypeException extends \InvalidArgumentException
{
    public function __construct($searchType)
    {
        $message = printf('Search type "%s" is invalid', $searchType);
        parent::__construct($message);
    }
}