<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class FutureDateException extends ResponseException
{
}