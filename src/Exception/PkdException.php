<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class PkdException extends ResponseException
{
    protected $defaultMsg = 'PKD not available.';
}