<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class NumberMissingException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        !$message && $message = 'No number provided';
        parent::__construct($message, $code, $previous);
    }
}