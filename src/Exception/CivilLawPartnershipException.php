<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class CivilLawPartnershipException extends ResponseException
{
    protected $defaultMsg = '';
}