<?php

namespace SJRoyd\GUS\RegonApi\Exception;

use SJRoyd\GUS\RegonApi\Request\SearchData;
use Throwable;

class TooManyNumbersException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $max = SearchData::MAX_NUMBERS_COUNT;;
        !$message && $message = "Too many numbers provided, {$max} expected.";
        parent::__construct($message, $code, $previous);
    }
}