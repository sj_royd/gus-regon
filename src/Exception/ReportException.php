<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class ReportException extends ResponseException
{
    protected $defaultMsg = 'No report Found.';
}