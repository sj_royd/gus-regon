<?php

namespace SJRoyd\GUS\RegonApi\Exception;

use Throwable;

class ResponseException extends \Exception
{

    protected $defaultMsg = '';

    public function __construct($message = "", $code = 0, $details = [])
    {
        $message = $this->prepareMessage($message, $details);
        parent::__construct($message, $code);
    }

    protected function prepareMessage($message, $details)
    {
        $c = [];
        foreach ($details as $k => $v) {
            if ($v) {
                $c[] = "{$k}: {$v}";
            }
        }

        return $message . ' (' . implode('; ', $c) . ')';
    }
}