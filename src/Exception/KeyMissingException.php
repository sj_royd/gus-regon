<?php

namespace SJRoyd\GUS\RegonApi\Exception;

use Throwable;

class KeyMissingException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        !$message && $message = 'Login key missging';
        parent::__construct($message, $code, $previous);
    }
}