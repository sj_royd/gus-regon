<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class PastDateException extends ResponseException
{
}