<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class NotFoundException extends ResponseException
{
    protected $defaultMsg = 'No data found.';
}