<?php

namespace SJRoyd\GUS\RegonApi\Exception;

class InvalidKeyException extends \InvalidArgumentException
{
    public function __construct($message = "")
    {
        !$message && $message = 'Invalid user key';
        parent::__construct($message);
    }
}